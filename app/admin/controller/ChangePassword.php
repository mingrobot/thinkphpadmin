<?php
namespace app\admin\controller;

use app\middleware\AdminAuth;
use think\facade\Config;

use think\facade\Session;
use think\facade\Db;

use think\App;
use think\Request;

/**
 * 修改密码
 * Class ChangePassword
 * @package app\admin\controller
 */
class ChangePassword 
{
	use \liliuwei\think\Jump;
	
	
	protected $middleware = [AdminAuth::class];
	
	
	public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
    }
	
	
    /**
     * 修改密码
     * @return mixed
     */
    public function index()
    {
		//var_dump(Config::get('app.salt'));exit;
        return view('system/change_password');
    }

    /**
     * 更新密码
     */
    public function updatePassword()
    {
        if ($this->request->isPost()) {
            $admin_id    = Session::get('admin_id');
            $data   = $this->request->param();
            $result = Db::name('admin_user')->find($admin_id);

            if ($result['password'] == md5($data['old_password'] . Config::get('app.salt'))) {
                if ($data['password'] == $data['confirm_password']) {
                    $new_password = md5($data['password'] . Config::get('app.salt'));
                    $res          = Db::name('admin_user')->where(['id' => $admin_id])->update(['password'=>$new_password]);

                    if ($res !== false) {
                        $this->success('修改成功');
                    } else {
                        $this->error('修改失败');
                    }
                } else {
                    $this->error('两次密码输入不一致');
                }
            } else {
                $this->error('原密码不正确');
            }
        }
    }
}