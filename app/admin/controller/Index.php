<?php
namespace app\admin\controller;


use think\facade\Db;
use think\Request;

use app\middleware\AdminAuth;


use think\App;
/**
 * 后台首页
 * Class Index
 * @package app\admin\controller
 */
//class Index extends AdminBase
class Index 
{
	
	protected $middleware = [AdminAuth::class];


    /**
     * 首页
     * @return mixed
     */
    public function index()
    {
		//$this->initialize();
		//return redirect('/admin/login/index');
        $version = Db::query('SELECT VERSION() AS ver');
        $config  = [
            'url'             => $_SERVER['HTTP_HOST'],
            'document_root'   => $_SERVER['DOCUMENT_ROOT'],
            'server_os'       => PHP_OS,
            'server_port'     => $_SERVER['SERVER_PORT'],
            'server_soft'     => $_SERVER['SERVER_SOFTWARE'],
            'php_version'     => PHP_VERSION,
            'mysql_version'   => $version[0]['ver'],
            'max_upload_size' => ini_get('upload_max_filesize')
        ];

        //return $this->fetch('index', ['config' => $config]);
		return view('index', ['config' => $config]);
    }
}