<?php
namespace app\admin\controller;

use app\common\model\Link as LinkModel;

use think\Request;


use think\facade\Db;
use app\middleware\AdminAuth;
use app\admin\validate\Link as validlink;

/**
 * 友情链接
 * Class Link
 * @package app\admin\controller
 */
class Link
{
   
	
	use \liliuwei\think\Jump;

 //protected $link_model;
	protected $middleware = [AdminAuth::class];

    /**
     * 友情链接
     * @return mixed
     */
    public function index()
    {
        $link_list = LinkModel::select();

        return view('index', ['link_list' => $link_list]);
    }

    /**
     * 添加友情链接
     * @return mixed
     */
    public function add()
    {
        return view();
    }

    /**
     * 保存友情链接
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            //$validate_result = $this->validate($data, 'Link');
			$validate_result = validate(validlink::class)->check($data);
			//var_dump($validate_result);exit;


            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
				$link_model = new LinkModel();
                if ($link_model->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑友情链接
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $link = LinkModel::find($id);

        return view('edit', ['link' => $link]);
    }

    /**
     * 更新友情链接
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = validate(Linkv::class)->check($data);

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
				$link_model = new LinkModel();
                if ($link_model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除友情链接
     * @param $id
     */
    public function delete($id)
    {
        if (LinkModel::destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}