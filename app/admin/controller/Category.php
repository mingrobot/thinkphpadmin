<?php
namespace app\admin\controller;

use app\common\model\Article as ArticleModel;
use app\common\model\Category as CategoryModel;


use app\middleware\AdminAuth;
use app\admin\validate\Category as validcategory;
use think\App;
use think\Request;
use think\facade\View;

/**
 * 栏目管理
 * Class Category
 * @package app\admin\controller
 */
class Category
{

	use \liliuwei\think\Jump;
	
	protected $middleware = [AdminAuth::class];
	
	
    protected $category_model;
    protected $article_model;

	public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;

		
        $this->category_model = new CategoryModel();
        $this->article_model  = new ArticleModel();
        $category_level_list  = $this->category_model->getLevelList();

        View::assign('category_level_list', $category_level_list);
    }

    /**
     * 栏目管理
     * @return mixed
     */
    public function index()
    {
        return view();
    }

    /**
     * 添加栏目
     * @param string $pid
     * @return mixed
     */
    public function add($pid = '')
    {
        return view('add', ['pid' => $pid]);
    }

    /**
     * 保存栏目
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data	= $this->request->param();
            //$validate_result = $this->validate($data, 'Category');
            $validate_result = validate(validcategory::class)->check($data);

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if ($this->category_model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑栏目
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $category = $this->category_model->find($id);

        return view('edit', ['category' => $category]);
    }

    /**
     * 更新栏目
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            //$validate_result = $this->validate($data, 'Category');

            $validate_result = validate(validcategory::class)->check($data);

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $children = $this->category_model->where(['path' => ['like', "%,{$id},%"]])->column('id');
                if (in_array($data['pid'], $children)) {
                    $this->error('不能移动到自己的子分类');
                } else {
                    if($datobj = $this->nav_model->find($id) ) {
                        if ($datobj->allowField(true)->save($data) !== false) {
                            $this->success('更新成功');
                        } else {
                            $this->error('更新失败');
                        }
                    }else{
                        $this->error('更新失败,未找到该条目');
                    }
                }
            }
        }
    }

    /**
     * 删除栏目
     * @param $id
     */
    public function delete($id)
    {
        $category = $this->category_model->where(['pid' => $id])->find();
        $article  = $this->article_model->where(['cid' => $id])->find();

        if (!empty($category)) {
            $this->error('此分类下存在子分类，不可删除');
        }
        if (!empty($article)) {
            $this->error('此分类下存在文章，不可删除');
        }
        if ($this->category_model->destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}