<?php
namespace app\admin\controller;

use app\common\model\SlideCategory as SlideCategoryModel;
use app\common\model\Slide as SlideModel;

use app\middleware\AdminAuth;
use app\admin\validate\Slide as validslide;

/**
 * 轮播图管理
 * Class Slide
 * @package app\admin\controller
 */
class Slide
{
    use \liliuwei\think\Jump;
    protected $middleware = [AdminAuth::class];

    /**
     * 轮播图管理
     * @return mixed
     */
    public function index()
    {
        $slide_category_model = new SlideCategoryModel();
        $slide_category_list  = $slide_category_model->column('name', 'id');
        $slide_list           = SlideModel::select();

        return view('index', ['slide_list' => $slide_list, 'slide_category_list' => $slide_category_list]);
    }

    /**
     * 添加轮播图
     * @return mixed
     */
    public function add()
    {
        $slide_category_list = SlideCategoryModel::select();

        return view('add', ['slide_category_list' => $slide_category_list]);
    }

    /**
     * 保存轮播图
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            //$validate_result = $this->validate($data, 'Slide');
            //validslide
            $validate_result = validate(validslide::class)->check($data);

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $slide_model = new SlideModel();
                if ($slide_model->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑轮播图
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $slide_category_list = SlideCategoryModel::select();
        $slide               = SlideModel::find($id);

        return view('edit', ['slide' => $slide, 'slide_category_list' => $slide_category_list]);
    }

    /**
     * 更新轮播图
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            //$validate_result = $this->validate($data, 'Slide');
            $validate_result = validate(validslide::class)->check($data);

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $slide_model = new SlideModel();
                if($datobj = $slide_model->find($id) ) {
                    if ($datobj->save($data) !== false) {
                        $this->success('更新成功');
                    } else {
                        $this->error('更新失败');
                    }
                }else{
                    $this->error('更新失败,未找到该条目');
                }

            }
        }
    }

    /**
     * 删除轮播图
     * @param $id
     */
    public function delete($id)
    {
        if (SlideModel::destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}