<?php
declare (strict_types = 1);

namespace app\middleware;

use think\facade\Session;
class PanelAuth
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //
		$panel_session = Session::get('panel_session');
		//var_dump($panel_session);exit;
		if( empty($panel_session) || $panel_session['outdate']<=time() ){
			return redirect('/panel/login/index');
        }
		return $next($request);
    }
}
