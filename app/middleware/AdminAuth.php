<?php
declare (strict_types = 1);

namespace app\middleware;

use think\facade\Session;
use think\facade\View;
use think\facade\Db;

use app\admin\model\Auth;

class AdminAuth
{
	
	use \liliuwei\think\Jump;
	
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $this->checkAuth();
        $this->getMenu();

        // 输出当前请求控制器（配合后台侧边菜单选中状态）
        //$this->assign('controller', Loader::parseName($this->request->controller()));
		//var_dump($request->controller());exit;
		View::assign('controller', $request->controller() );
		
		return $next($request);
    }
	
	

    /**
     * 权限检查
     * @return bool
     */
    protected function checkAuth()
    {
		//var_dump(Session::get('admin_id'));
		//echo 3;exit;
        if ( empty(Session::get('admin_id')) ) {
            //redirect('admin/login/index');
			//return redirect('/admin/login/index');
			
			$this->error('未登录或登录失效');
        }
		


        $module     = app('http')->getName();
        $controller = $this->request->controller();
        $action     = $this->request->action();

        // 排除权限
        $not_check = ['admin/Index/index', 'admin/AuthGroup/getjson', 'admin/System/clear'];

        if (!in_array($module . '/' . $controller . '/' . $action, $not_check)) {
            $auth     = new Auth();
            $admin_id = Session::get('admin_id');
            if (!$auth->check($module . '/' . $controller . '/' . $action, $admin_id) && $admin_id != 1) {
                $this->error('没有权限');
            }
        }
    }

    /**
     * 获取侧边栏菜单
     */
    protected function getMenu()
    {
        $menu     = [];
        $admin_id = Session::get('admin_id');
        $auth     = new Auth();
        //var_dump($admin_id);exit;
        //->fetchSql(true)
        $auth_rule_list = Db::name('auth_rule')->where('status', 1)->order(['sort' => 'DESC', 'id' => 'ASC'])->select();

        //var_dump($auth_rule_list);exit;
        foreach ($auth_rule_list as $value) {
            if ($auth->check($value['name'], $admin_id) || $admin_id == 1) {
                $menu[] = $value;
            }
        }
        $menu = !empty($menu) ? array2tree($menu) : [];

		//var_dump($menu);exit;
        View::assign('menu', $menu);
    }
	
}
