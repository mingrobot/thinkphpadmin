<?php /*a:1:{s:47:"D:\workplace\tp6\app\Home\view\index\index.html";i:1583502189;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8">
  <title>云采集助手 采集工具 采集大狮工作室出品</title>
  <meta name="keywords" content="采集工具，采集联系方式，内容采集，营销资源，云采集助手，互联网采集，采集大狮工作">
  <meta name="description" content="采集大狮工作室推出云采集助手，采用本地采集云端解析模式，适应当前互联网采集模式，无需频繁升级，让营销资源唾手可得, 面向中小企业、个人创业者、科研和数据分析工作者，让每个人都拥有数据化能力">
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-status-bar-style" content="black"> 
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="format-detection" content="telephone=no">
  
  <link rel="stylesheet" href="/static/layui/css/layui.css?t=1"  media="all">
  <link rel="stylesheet" href="/static/css/indexglobal.css?t=1" media="all">
</head>




<body class="site-home" id="LAY_home" style="background-color: #eee;" data-date="2-23">



<div class="layui-header header header-index" spring>
  <div class="layui-main">
    <a class="logo" href="/">
      <font style="color: rgba(255,255,255,.7);font-size: 28px;">采集大狮</font>
    </a>

    <ul class="layui-nav">
      <li class="layui-nav-item ">
        <a href="/">说明<!-- <span class="layui-badge-dot"></span> --></a> 
      </li>
      <li class="layui-nav-item ">
        <a href="/">会员<!-- <span class="layui-badge-dot"></span> --></a>
      </li>
      

      
      <li class="layui-nav-item">
        <a href="javascript:;"><!--<span class="layui-badge-dot" style="left:0; right: auto; margin: -4px 0 0 5px;"></span>--> 目录</a>
        <dl class="layui-nav-child" style="left: auto; right: -22px; text-align: center;">
          <dd lay-unselect>
            <a href="/" target="_blank">其他工具</a>
          </dd>
          <dd lay-unselect>
            <a href="/" target="_blank">数据市场</a>
            <hr>
          </dd>
          
          <dd class="layui-hide-sm layui-show-xs" lay-unselect>
            <a href="/" target="_blank">社区交流</a>
            <hr>
          </dd>

          </dd>

        </dl>
      </li>
      
      
      
      <li class="layui-nav-item layui-hide-xs" lay-unselect>
        <a href="/panel/" target="_blank">登录 <span class="layui-badge-dot" style="margin-top: -5px;"></span></a>
		
		
      </li>
    </ul>
  </div>
</div>


<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



<div class="site-banner">
  <div class="site-banner-bg" style="background-image: url(/static/images/spring.jpg); background-size: cover;">
  </div>
  <div class="site-banner-main">
    <div class="site-zfj">
      <i class="layui-icon" style="color: #fff; color: rgba(255,255,255,.7);">&#xe715;</i>
    </div>
    <div class="layui-anim site-desc">
      <p class="web-font-desc">云采集助手</p>
      <cite>面向中小企业、个人创业者、科研和数据分析工作者，采集大狮工作室推出云采集助手，本地采集云端解析，让每个人都拥有数据化能力</cite>
    </div>
    <div class="site-download">
      <a href="#" class="layui-inline site-down" target="_blank">
        <cite class="layui-icon">&#xe601;</cite>
        立即下载，版本更新中，敬请期待
      </a>
    </div>
    <div class="site-version">
      <span>当前版本：<cite class="site-showv">2.0</cite></span>
      <span><a href="#" rel="nofollow" target="_blank">更新日志</a></span>
      <span>下载量：<em class="site-showdowns">613</em></span>
    </div>
	
    <div class="site-banner-other">
    </div>
  </div>
</div>



<div class="layui-main">

  <ul class="site-idea">
    <li>
      <fieldset class="layui-elem-field layui-field-title">
        <legend>数据化时代的要求</legend>
        <p>在数据化在当今社会扮演越来越重要角色的时候，企业营销，市场决策，科学研究都离不开掌握数据，一个合适的数据采集工具让你快人一步</p>
      </fieldset>
    </li>
    <li>
      <fieldset class="layui-elem-field layui-field-title">
        <legend>本地采集云端解析</legend>
        <p>你是否遇到过采集工具配置繁琐，容易失效，效率很低，采集大狮工作室推出云采集助手，首创本地采集云端解析模式，让你的工具实时更新</p>
      </fieldset>
    </li>

  </ul>  
</div>



<div class="layui-footer footer footer-index">
  <div class="layui-main">
    <p> <a href="/">采集大狮工作室</a> 版权所有&copy; 2020，联系客服QQ715829106</p>
	
	<!--
    <p>
      <a href="/" target="_blank">案例</a>
      <a href="/" target="_blank">支持</a>

    </p>
	-->

  </div>
</div>

<div class="site-tree-mobile layui-hide">
  <i class="layui-icon">&#xe602;</i>
</div>
<div class="site-mobile-shade"></div>
<script src="/static/layui/layui.js?t=1" charset="utf-8"></script>
<script>
window.global = {
  pageType: 'index'
  ,preview: function(){
    var preview = document.getElementById('LAY_preview');
    return preview ? preview.innerHTML : '';
  }()
};
layui.config({
  base: '/static/layui/lay/modules/'
  ,version: '21'
}).use('global');
</script>
 


</body>
</html>