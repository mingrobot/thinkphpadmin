<?php /*a:1:{s:48:"D:\workplace\tp6\app\panel\view\login\index.html";i:1582794180;}*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>用户登录 - 采集大狮 欢迎您</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Content-Language" content="zh-cn"/>
<meta name="author" content="mingrobot#mail.com"/>
<meta name="copyright" content="bluecart.net"/>
<script type="text/javascript" src="/adm/jquery-1.4.4.min.js"></script>
<script language="javascript" type="text/javascript" src="/adm/date/WdatePicker.js"></script>
<script type="text/javascript" src="/adm/xheditor-1.1.13-zh-cn.min.js"></script>

<style>

html, body {
    color: #000000;
    font: 12px/1.8 'Microsoft YaHei',Tahoma,Verdana,'Simsun';
background:#fbfbfb;}
a {
    color: #1C57C4;
    text-decoration: underline;
}


#login {
    color: #333333;
    margin: 0;
    padding: 140px 0 0;
}
.login-lang {
}

.login-min {
    margin: 0 auto;
    padding: 20px 0;
    width: 600px;
}
.login-left {
    border-right: 1px solid #CCCCCC;
    float: left;
    margin-top: 40px;
    text-align: center;
    width: 270px;
}
.login-left p {
    margin-top: 10px;
    padding-left: 5px;
    text-align: center;
}
.login-left a.img {
}
.login-right {
    border-left: 1px solid #FFFFFF;
    float: left;
    padding-left: 10px;
    width: 305px;
}
.login-title {
    font-size: 16px;
    height: 35px;
    line-height: 35px;
    margin-bottom: 8px;
    overflow: hidden;
    padding-left: 85px;
}
.login-right input.text {
    height: 20px;
    line-height: 20px;
    margin: 0;
    width: 150px;
}
.login-right p {
    margin: 10px 0;
}
.login-right form label {
    float: left;
    font-size: 13px;
    font-weight: bold;
    padding-right: 10px;
    text-align: right;
    width: 76px;
}
.login-submit {
    padding-left: 86px;
}
.login-code img {
    height: 30px;
    width: 100px;
}
.login-code input.text {
    height: 20px;
    line-height: 20px;
    margin: 0;
    width: 80px;
}
.login-submit {
    padding-top: 5px;
}
.login-submit input {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: none repeat scroll 0 0 #B5DFE9;
    border-color: -moz-use-text-color #71A9C5 #71A9C5 -moz-use-text-color;
    border-image: none;
    border-right: 1px solid #71A9C5;
    border-style: none solid solid none;
    border-width: 0 1px 1px 0;
    color: #333333;
    cursor: pointer;
    margin-right: 10px;
    padding: 5px 12px;
}
.login-submit a {
    margin-right: 8px;
}
.login-zhao {
    padding-right: 15px;
    text-align: right;
}
.login-zhao a {
    color: #1C57C4;
    margin: 0 20px;
    text-decoration: underline;
}
.login-zhao a:hover {
    color: #1C57C4;
    text-decoration: underline;
}
#pass1, #pass2 {
    color: #FF0000;
    padding-left: 6px;
}

.footer {
    color: #777777;
    font-size: 12px;
    overflow: hidden;
    padding: 8px 0;
    text-align: center;
}

.clear {
    clear: both;
    font-size: 0;
    height: 0;
    overflow: hidden;
}
</style>

<script type="text/javascript">
function check_main_login(){
	var name = $("input[name='login_name']");
	var pass = $("input[name='login_pass']");
		if(name.val() == ''){
			alert('用户名不能为空');
			name.focus();
			return false;
		}
		if(pass.val() == ''){
			alert('密码不能为空');
			pass.focus();
			return false;
		}
}
function pressCaptcha(obj){
    obj.value = obj.value.toUpperCase();
}

</script>

</head>
<body id="login">
<div class="login-min">
			<div class="login-left">
				<div style=" border-right:1px solid #fff; padding:0px 0px 20px;">
				<a href="/" style="font-size:0px;" target="_blank" title="网站管理系统" class="img">
					<img src="/adm/bluecart/caijidashi.png" width="250px" alt="管理系统" title="管理系统" />
				</a>
				<!--
				<p>具有营销价值的数据采集系统</p>
				-->
				</div>
			</div>
			
			
			<div class="login-right">	
				<h1 class="login-title">用户登录</h1>
				<div>
				<form method="post" action="/panel/login/index" name="main_login" onSubmit="return check_main_login()">
				
				<p style="height:22px; margin-top:0px;">
				<!--
					<label>后台语言</label>
					<select name="loginlang" onchange=javascript:window.location.href=this.options[this.selectedIndex].value >
				<option value="cn" selected='selected'>简体中文</option>
					</select>
				-->
				</p>
				
					<p><label>用户名</label><input type="text" class="text" name="login_name"   /></p>
					<p><label>密码</label><input type="password" class="text" name="login_pass" /></p>
					
					<?php if($havevcode): ?>
					<p class="login-code">
						<label>验证码</label>
						<input type="text" id="code" class="text mid" name="code">
						<img align="absbottom" title="点击刷新验证码" style="cursor: pointer;" onclick="this.src='/panel/login/getValidateCode?tm='+Math.random();" src="/panel/login/getValidateCode">

					</p>
					<?php endif; ?>
					
					<p class="login-submit">
					<?php if($err): ?><span style="background-color: #cc0000; color: #fce94f; font-size:16px;"><?php echo htmlentities($err); ?></span><?php endif; ?>
						<input type="submit" name="Submit" value="登录" /> 
						
						<span style="background-color: #cc0000; color: #fce94f; font-size:16px;">请使用账号：guest，密码123456 进行验证测试</span>
						<!--<a href="/login/findpwd">忘记密码?</a>-->
					</p>
				</form>
				</div>
			</div>
		<div class="clear"></div>
</div>
<div class="footer" style="margin-top:80px;">
 <p class="copyright">
<span class="name">caijidashi v2.0</span> | © 2020 采集大狮工作室 
</p>
</div>
</body>
</html>
