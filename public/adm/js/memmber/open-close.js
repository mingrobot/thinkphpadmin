

function D1(obj){
  var h=obj.offsetHeight;
  var maxh=120;
  function dmove(){
    h+=20; //设置层展开的速度
    if(h>=maxh){
      obj.style.height=maxh+'px';
      clearInterval(iIntervalId);
    }else{
      obj.style.display='block';
      obj.style.height=h+'px';
    }
  }
  iIntervalId=setInterval(dmove,2);
}

function D2(obj){
  var h=obj.offsetHeight;
  var maxh=120;
  function dmove(){
    h-=10;//设置层收缩的速度
    if(h<=0){
      obj.style.display='none';
      clearInterval(iIntervalId);
    }else{
      obj.style.display='block';
      obj.style.height=h+'px';
    }
  }
  iIntervalId=setInterval(dmove,2);
}

function use(obj){
  var obj=$('#'+obj)[0];
  var sb=$('#stateBut')[0];
  if(obj.style.display=='none'){
    D1(obj);
    sb.className='btn_close';
  }else{
    D2(obj);
    sb.className='btn_open';
  }
}