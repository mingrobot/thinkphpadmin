	function fixTableHeader(gv,scrollHeight){
		var gvn=$(gv).clone(true).removeAttr("id");
		$(gvn).find("tr:not(:first)").remove();
		$(gv).before(gvn);
		$(gv).find("tr:first").remove();
		$(gv).wrap("<div style='height:"+ scrollHeight +"px; overflow-y: scroll;overflow-x:hidden; overflow: auto; padding: 0;margin: 0;'></div>");
		if($(gv).attr('offsetHeight') <= scrollHeight){
			$('#td_scroll').hidden();
		}
	}
//	$(function(){
//		fixTableHeader("#table1",450);
//	});