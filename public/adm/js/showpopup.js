function showpopup()
{  
var Obj = document.getElementById("divmask");
if (!Obj) $("body").append("<div id='divmask' style='display:none'></div>");

$(".pop_close").click(function(){
    $("#divmask").hide();
    $('#consume_pop').hide();
    return false;
});

    var arrPageSizes = ___getPageSize();
$('#divmask').css({
backgroundColor:	'#000',
opacity:			0.3,
left:               0,
top:                0,
"z-index":          5,
position:           'absolute',
width:				arrPageSizes[0],
height:				arrPageSizes[1]
}).show();
var arrPageScroll = ___getPageScroll();
var popupwidth = parseInt($('#consume_pop').width()/2);
$('#consume_pop').css({
"z-index": 6,
display:  'block',
top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
left:	parseInt((arrPageSizes[0]-arrPageScroll[0])/2,10)-popupwidth
}).show();

$(window).resize(function() 
{
var arrPageSizes = ___getPageSize();
$('#divmask').css({
width:		arrPageSizes[0],
height:		arrPageSizes[1]
});

var arrPageScroll = ___getPageScroll();
$('#consume_pop').css({
top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
left:	parseInt((arrPageSizes[0]-arrPageScroll[0])/2,10)-popupwidth
});
});

return false;
}

function ___getPageScroll() {
var xScroll, yScroll;
if (self.pageYOffset) {
yScroll = self.pageYOffset;
xScroll = self.pageXOffset;
} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
yScroll = document.documentElement.scrollTop;
xScroll = document.documentElement.scrollLeft;
} else if (document.body) {// all other Explorers
yScroll = document.body.scrollTop;
xScroll = document.body.scrollLeft;	
}
arrayPageScroll = new Array(xScroll,yScroll);
return arrayPageScroll;
};
function ___getPageSize() {
var xScroll, yScroll;
if (window.innerHeight && window.scrollMaxY) {	
xScroll = window.innerWidth + window.scrollMaxX;
yScroll = window.innerHeight + window.scrollMaxY;
} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
xScroll = document.body.scrollWidth;
yScroll = document.body.scrollHeight;
} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
xScroll = document.body.offsetWidth;
yScroll = document.body.offsetHeight;
}
var windowWidth, windowHeight;
if (self.innerHeight) {	// all except Explorer
if(document.documentElement.clientWidth){
windowWidth = document.documentElement.clientWidth; 
} else {
windowWidth = self.innerWidth;
}
windowHeight = self.innerHeight;
} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
windowWidth = document.documentElement.clientWidth;
windowHeight = document.documentElement.clientHeight;
} else if (document.body) { // other Explorers
windowWidth = document.body.clientWidth;
windowHeight = document.body.clientHeight;
}	
// for small pages with total height less then height of the viewport
if(yScroll < windowHeight){
pageHeight = windowHeight;
} else { 
pageHeight = yScroll;
}
// for small pages with total width less then width of the viewport
if(xScroll < windowWidth){	
pageWidth = xScroll;		
} else {
pageWidth = windowWidth;
}
arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
return arrayPageSize;
};
